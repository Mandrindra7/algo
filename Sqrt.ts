//calcul sqrt of number : https://leetcode.com/problems/sqrtx/
//Input: x = 4
//Output: 2


//using sqrt javascript Math function
const  mySqrt = function(x: number) {
    const res: any =  Math.sqrt(x)
    return parseInt(res) 
 };

 //using binary search
 const mySqrtBS = function(x) {
    let start = 0;
    let end = x;
    let res = 0;
    while (start < end) {
        res = Math.ceil((start + end)/2);
        if (res*res <= x && (res + 1)*(res + 1) > x) return res; 
        if (res*res < x) start = res; 
        else end = res; 
    }
    return res;
};