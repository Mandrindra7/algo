// https://leetcode.com/problems/two-sum/
// Input: nums = [3,2,4], target = 6
// Output: [1,2]


//using indexOf
const twoSum = function(nums: number[], target: number) {
    const res: number[] =[]
     for(let i = 0; i < nums.length; i++){
         if(nums.indexOf(target-nums[i]) !== -1 && nums.indexOf(target-nums[i]) !== i){
             res.push(i)
             res.push(nums.indexOf(target - nums[i]))
            break;
         }
     }
    
    return res
};

//using map
const twoSumMap = (nums: number[], target: number) => {
    const map = new Map();
    let sum : number; // the number to which an addend is added (mathematical and computer science term for the left number in addition, the right number is the addend)
    
    for(let i = 0; i < nums.length; i++) {
        sum = target - nums[i];
        if(map.has(sum)){
            return [map.get(sum), i];
        }
        map.set(nums[i], i);
    }
};